(define-module (network stis message simple)
  #:use-module (network stis message messenger)
  #:export (make-server make-server/proxy
            make-prober 
            make-client make-client/proxy
            prefix-e start-e end-e))

(define prefix-e #'prefix-e)
(define start-e  #'start-e)
(define end-e    #'end-e)
(define-syntax add-id
  (syntax-rule ()
               ((nm (a b ...))
     (with-syntax ((id (datum->syntax #'nm (gensym "id"))))
       (a id b ...)))))

(define-syntax-rule (make-server (add send loop) (addr-in addr-out))
  (add-id
   (make-messenger ((#:sub block-read   addr-in)
                    (#:pub publish      addr-out))                  
                   (lambda ()
                     (let ()
                       (lambda (lp)
                         (listener! publish global-e)
                         (let loop ()
                           (if (finish?)
                               'ok
                               (begin
                                 (block-read)
                                 (lp)
                                 (loop))))))))))

(define-syntax-rule (make-server/proxy (add send loop) (addr-in addr-out))
  (add-id
   (make-messenger ((#:sub       block-read   addr-in)
                    (#:pub-proxy publish      addr-out))                  
                   (lambda ()
                     (let ()
                       (lambda (lp)
                         (listener! publish global-e)
                         (let loop ()
                           (if (finish?)
                               'ok
                               (begin
                                 (block-read)
                                 (lp)
                                 (loop))))))))))

(define-syntax-rule (make-prober (add send loop) (addr-in))
  (add-id
   (make-messenger ((#:sub block-read   addr-in)
                    (lambda ()
                      (let ()
                        (lambda (lp)
                          (let loop ()
                            (if (finish?)
                                'ok
                                (begin
                                  (block-read)
                                  (lp)
                                  (loop)))))))))))
   

(define-syntax-rule (make-client (add send loop) (addr-in addr-out))
  (add-id
   (make-messenger ((#:sub block-read   addr-in)
                    (#:pub publish      addr-out))                  
                   (lambda ()
                     (lambda (lp)
                       (listener! publish global-e)
                       (let loop ()
                         (if (finish?)
                             (begin
                               (send start-e)
                               (block-read)
                               (lp)
                               (send end-e)
                               (loop)))))))))

(define-syntax-rule (make-client/proxy (add send loop) (addr-in addr-out))
  (add-id
   (make-messenger ((#:sub       block-read   addr-in)
                    (#:pub-proxy publish      addr-out))                  
                   (lambda ()
                     (lambda (lp)
                       (listener! publish global-e)
                       (let loop ()
                         (if (finish?)
                             (begin
                               (send start-e)
                               (block-read)
                               (lp)
                               (send end-e)
                               (loop)))))))))
