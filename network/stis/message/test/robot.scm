(define start-e '(start))

(make-event-loop listener! send run
   (lambda ()
     (let ((time 0)
           (finish? #f))       
       (let ((dt   0.1))
         (lambda (lp)
           (let loop ()
             (if (finish?)
                 'ok
                 (begin
                   (send start-e)
                   (lp)
                   (time (+ (time) dt))
                   (loop)))))))))

(define robot-e '(robot))

(define-syntax-rule (mk-robot nm nm-e x0)
  (begin
    (define nm-e (make-event nm ((x #f))))
    (define nm
      (let ((state 'start))
        (lambda (message time finish?)
          (and (eq? state 'start)
               (> (time) x0)
               (begin
                 (send robot-e (o nm-e x0))
                 (set! state 'end))))))))


(define (last es time finish?)
  (when (> (time) 100) (finish? #t)))
      
(mk-robot rob rob-e 10)
(mk-robot bob bob-e 15)

(define (robot-log e t f)
  (match e
    ((_ (r . _))
     (format #t "robot log: caught robot ~a at ~a~%" r (t)))))

(define-syntax-rule (mk-robot-logger nm robot)
  (define (nm e t f)
    (match e
      ((_ (r . _))
       (format #t "robot log (~a): caught robot at ~a~%" 'robot (t))))))

(mk-robot-logger robot-rob-log rob)
(mk-robot-logger robot-bob-log bob)


(listener! last start-e)
(listener! rob  start-e)
(listener! bob  start-e)

(listener! robot-log     robot-e)
(listener! robot-rob-log robot-e rob-e)
(listener! robot-bob-log robot-e bob-e)
(run)
