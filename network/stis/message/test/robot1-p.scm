(use-modules (network stis message))

(define start-e    '(start))
(define finish-e   '(finish))
(define global-e   '(global))

(make-messenger mess1 ((#:pub-proxy proxy-pub-e   "tcp://localhost:5551"))
    (listener! send run)
    (lambda ()
      (let ((time 0)
            (finish? #f))       
        (lambda (lp)
          (let ((dt   0.1))
            (listener! proxy-pub-e global-e)
            (let loop ()
              (if (finish?)
                  'ok
                  (begin
                    (send global-e start-e)
                    (usleep 10000)
                    (pk (time))
                    (lp)
                    (time (+ (time) dt))
                    (loop)))))))))

(define robot-e '(robot))
(define-syntax-rule (mk-robot nm nm-e x0)
  (begin
    (define nm-e (make-event nm ((x #f) (t 0))))
    (define nm
      (let ((state 'start))
        (lambda (message time finish?)          
          (and (eq? state 'start)
               (> (time) x0)
               (begin
                 (send global-e robot-e (o nm-e x0 (time)))
                 (set! state 'end))))))))


(define (last es time finish?)
  (when (> (time) 100) 
    (send global-e finish-e)
    (finish? #t)))
      
(mk-robot rob rob-e 10)
(mk-robot bob bob-e 15)
(listener! (lambda x (pk x)) global-e)
(listener! last global-e start-e)
(listener! rob  global-e start-e)
(listener! bob  global-e start-e)

(run)
