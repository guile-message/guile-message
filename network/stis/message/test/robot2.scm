(use-modules (network stis message))
(use-modules (ice-9 match))

(define global-e     '(global))
(define read-e       '(read))
(define read-block-e '(read-block))
(make-messenger mess2 
               ((#:pub proxy-pub "tcp://*:5552") 
                (#:sub block     "tcp://localhost:5551"))
               (listener! send run)
   (lambda ()
     (let ((finish? #f))       
       (lambda (lp)
         (listener! block  read-block-e)
         (let loop ()
           (if (finish?)
               'ok
               (begin
                 (send read-block-e)
                 (lp)
                 (loop))))))))

(define (finish x f?)
  (f? #t))


(define (robot-log e f)
  (match e
    ((_ _ (_ r t . _))
     (format #t "robot log: caught robot ~a at ~a~%" r t))))

(define-syntax-rule (mk-robot-logger nm robot)
  (define (nm e f)
    (match e
      ((_ _ (_ r t . _))
       (format #t "robot log (~a): caught robot at ~a~%" 'robot t)))))

(mk-robot-logger robot-rob-log rob)
(mk-robot-logger robot-bob-log bob)

(listener! finish         global-e '(finish))
(listener! robot-log      global-e '(robot))
(listener! robot-rob-log  global-e '(robot) '(rob))
(listener! robot-bob-log  global-e '(robot) '(bob))
(run)


