(define-module (network stis message messenger)
  #:use-module (network stis message event-loop)
  #:use-module (ice-9   match)
  #:use-module (network zmq)
  #:use-module (syntax parse)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 pretty-print)
  #:export (make-messenger))
                
;-------------------------------------------------------------------------------
(define (scheme->bv l)
  (string->utf8
   (call-with-output-string
    (lambda (p) (write l p)))))

(define (bv->scheme bv)
  (let ((s (utf8->string bv)))
    (if (> (string-length s) 0)
        (call-with-input-string s
           (lambda (p) (read p)))
        #f)))
        

(define (px x)
  (pretty-print 
   (let loop ((x x))
     (syntax-case x ()
       ((x . l)
        (cons (loop #'x) (loop #'l)))
       (x (syntax->datum #'x)))))
  x)

(define-syntax-class lam-cl  
  #:description "lambda or lambda*"
  (pattern (~or (~datum lambda*) (~datum lambda))))

(define-syntax-class let-cl
  #:description "let or let*"
  (pattern (~or (~datum let*) (~datum let))))

(define-syntax-class sub-cl
  (pattern (#:sub (sub-e:id sub-block-e:id) ~! addr:expr ...)
           #:attr code-fkn
           (lambda (nm ctx send rest) 
             #`(with-zmq-socket (socket #,ctx ZMQ_SUB) 
                  (define (sub-block-e m . l)
                    (let ((m (bv->scheme 
                              (zmq-recv socket))))
                      (and m
                           (not (eq? (car m) #,nm))
                           (apply #,send (cdr m)))))
                  
                  (define (sub-e m . l)
                    (let ((m (bv->scheme 
                              (zmq-recv socket
                                        ZMQ_NOBLOCK))))
                      (and m
                           (not (eq? (car m) #,nm))
                           (apply #,send (cdr m)))))
                  
                  (zmq-connect socket addr) ...
                  (zmq-setsockopt socket ZMQ_SUBSCRIBE "")
                  #,rest)))

  (pattern (#:sub sub-block-e:id addr:expr ...)
           #:attr code-fkn
           (lambda (nm ctx send rest) 
             #`(with-zmq-socket (socket #,ctx ZMQ_SUB) 
                  (define (sub-block-e m . l)
                    (let ((m (bv->scheme 
                              (zmq-recv socket))))
                      (and m
                           (not (eq? (car m) #,nm))
                           (apply #,send (cdr m)))))
                  
                  (zmq-connect socket addr) ...
                  (zmq-setsockopt socket ZMQ_SUBSCRIBE "")
                  #,rest))))

(define-syntax-class pub-cl
  (pattern (#:pub-proxy ~! pub-e:id addr:str ...)
           #:attr code-fkn
           (lambda (nm ctx send rest) 
             #`(with-zmq-socket (socket #,ctx ZMQ_PUB) 
                 (define (pub-e m . l)
                   (zmq-send socket (scheme->bv (cons #,nm m))))
                 (zmq-connect socket addr) ...
                 #,rest)))

  (pattern (#:pub ~! pub-e:id addr:str ...)
           #:attr code-fkn
           (lambda (nm ctx send rest) 
             #`(with-zmq-socket (socket #,ctx ZMQ_PUB) 
                 (define (pub-e m . l)
                   (zmq-send socket (scheme->bv (cons #,nm m))))
                 (zmq-bind socket addr) ...
                 #,rest))))

(define-syntax-class sock-cl
  (pattern ((~or x:pub-cl y:sub-cl) ...)
           #:attr code
           (lambda (id ctx send code)
             (let loop ((fs (append x.code-fkn y.code-fkn)) (c code))
               (if (pair? fs)
                   (loop (cdr fs) ((car fs) id ctx send c))
                   c)))))
                                       
(define-syntax make-messenger
  (lambda (x)
    (syntax-parse x
      ((_ n:id ctx:id ~! scks:sock-cl
          (add:id send:id loop:id)
          (lambda-:lam-cl args
            (let-:let-cl x 
              ((~datum lambda) (lp:id)
                 code ...))))

       (with-syntax  ((nm  (datum->syntax #'1 (gensym "nm")))
		      (ctx (datum->syntax #'1 (gensym "ctx"))))
       #`(make-event-loop add send loop
           (lambda- args
             (let- x
               (lambda (lp)
                 (let ((nm 'n))
                   #,(scks.code #'nm #'ctx #'send 
                                #'(let () code ...)))))))))


      ((_ n:id scks:sock-cl
          (add:id send:id loop:id)
          (lambda-:lam-cl args
            (let-:let-cl x 
              ((~datum lambda) (lp:id)
                 code ...))))
       
       (with-syntax  ((nm  (datum->syntax #'1 (gensym "nm")))
		      (ctx (datum->syntax #'1 (gensym "ctx"))))

       #`(make-event-loop add send loop	   
           (lambda- args
             (let- x
               (lambda (lp)
                 (let ((nm 'n))
                   (with-zmq-context (ctx)
                      #,(scks.code #'nm #'ctx #'send 
                                   #'(let () code ...)))))))))))))
