(define-module (network stis message event-loop)
  #:use-module (ice-9  match)
  #:use-module (syntax id-table)
  #:export (make-event-loop make-event o))
                
;-------------------------------------------------------------------------------

#|
Hash table handling, 

In a sequence of events, the id part of the event has a syntax object
of an identifier a proper map will be used.
|#

(define (my-make-hash-table)
  (cons 
   (make-hash-table)
   (make-free-id-table)))

(define* (my-hash-ref h x #:optional (e #f))
  (if (pair? h)
      (let ((v (hash-ref (car h) x e)))
        (if (eq? v e)
            (free-id-table-ref (cdr h) x e)
            v))
      (hash-ref h x e)))

(define* (my-hash-set! h x v)
  (if (pair? h)
      (if (and (vector? x) (eq? 'syntax-object (vector-ref x 0)))
          (free-id-table-set! (cdr h) x v)
          (hash-set!          (car h) x v))
      (hash-set! h x v)))

(define (my-hash-for-each f h)
  (if (pair? h)
      (begin
        (hash-for-each f          (car h))
        (free-id-table-for-each (cdr h) f))
      (hash-for-each h f)))

(define (get-id e)
  (if (pair? e) 
      (car e) 
      e))

(define (make-node e)
  (list e '() (my-make-hash-table)))

(define (tree-insert-in-new-node h f e l)
  (let ((r (make-node e)))
    (my-hash-set! h (car e) r)
    (tree-insert f r l)))

(define (tree-insert f r l)
  (let* ((e  (car l))
         (l  (cdr l)))
    (if (null? l)
        (match r
          ((e (and lis (set! lis-set!)) childs)
           (lis-set! (cons f lis))))
        (match r
          ((e lis childs)
           (let* ((e2  (car l))
                  (l2  (cdr l))
                  (r2  (my-hash-ref childs (car e2) #f)))
             (if r2
                 (tree-insert f r2 l)
                 (tree-insert-in-new-node childs f e2 l))))))))
                 
            

(define (make-es es l)
  (let loop ((es es) (r '()) (a #f))
    (match es
      (((and x (_ . e)) . l)
       (if (procedure? e)
           (loop (cons (e) l) r a)
           (loop l (cons x r) a)))
      (() 
       (if a
           (reverse r)
           (loop l (reverse r) #t))))))
      
(define-syntax-rule (make-listener-f listeners)
  (lambda (f e . l)
    (let ((r (my-hash-ref listeners (get-id e) #f)))
      (if r
          (tree-insert f r (cons e l))
          (tree-insert-in-new-node listeners f e (cons e l))))
    listeners))

(define-syntax-rule (make-message-f listeners post)
  (lambda (e . l)
    (let* ((id (get-id e))
           (r  (my-hash-ref listeners id #f)))
      (unless r (error "wrong message format"))
      (let find-listeners ((r r) (e e) (m '()) (l l))
        (match r
          ((_ lis childs)
           (let* ((es (make-es (cons e m) l))
                  (coll (lambda ()
                          (for-each 
                           (lambda (f)
                             (set! post (cons (cons f es) post)))
                           (reverse lis)))))
             (match l
               (()                
                (my-hash-for-each 
                 (lambda (k r)
                   (find-listeners r (car r) 
                                   (cons e m) l))
                 childs)
                (coll))

               ((e2 . l2)
                (let ((r (my-hash-ref childs (get-id e2) #f)))
                  (if r 
                      (begin
                        (find-listeners r e2 (cons e m) l2)
                        (coll))
                      (coll))))))))))))
                                   
(define-syntax-rule (make-event-loop add-listener send-message run
                        (lambda- args
                          (let- ((let-s let-v) ...) code ...)))
  (begin
    (define add-listener #f)
    (define send-message #f)
    (define run          #f)
    (define u72 
      (begin
        (let ((pre       '())
              (post      '())
              (listeners (my-make-hash-table)))
          (set! add-listener (make-listener-f listeners))
          (set! send-message (make-message-f listeners post))
          (set! run
                (lambda- args
                   (let- ((let-s let-v) ...)
                         (let ((let-s (case-lambda
                                        (()  let-s)
                                        ((v) (set! let-s v))))
                               ...)
                        
                           ((begin code ...)
                            (lambda ()
                              (let loop ()
                                (if (and (null? pre) (null? post))
                                    'finished
                                    (begin
                                      (when (null? pre)
                                        (set! pre (reverse post))
                                        (set! post '()))
                                      (let ((f (car pre)))
                                        (set! pre (cdr pre))
                                        ((car f) (cdr f) let-s ...)
                                        (loop))))))))))))
        #f))))

(define (o m . l)
  (if (and (pair? m) (procedure? (cdr m)))
      (apply (cdr m) l)
      (error "could not apply event")))

(define-syntax make-event0
  (syntax-rules ()
    ((_ nm (a ...))
     (list nm a ...))
    ((_ nm (a ...) ((s v) ...) . l)
     (cons nm (lambda* (#:optional (s v) ...)
                       (make-event0 nm (a ... s ...) . l))))))
       
(define-syntax make-event
  (syntax-rules ()
    ((_ nm)
     (list nm))
    ((_ nm . l)
     (make-event0 nm () . l))))

