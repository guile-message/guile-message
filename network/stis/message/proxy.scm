(define-module (network stis message proxy)
  #:use-module (network zmq)
  #:export (start-messenger-proxy))

(define (start-messenger-proxy sub-addr pub-addr)
   (with-zmq-context (ctx)
      (with-zmq-socket (s-sub ctx ZMQ_XSUB)
        (with-zmq-socket (s-pub ctx ZMQ_XPUB)                         
              (zmq-bind s-sub sub-addr)
              (zmq-bind s-pub pub-addr)
              (zmq-proxy s-sub s-pub)))))
                 
                 
