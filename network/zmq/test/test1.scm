(use-modules (network zmq))
(use-modules (rnrs bytevectors))

(with-zmq (ctx) (requester ZMQ_REQ)
   (zmq-connect requester "tcp://localhost:5550")
   (let loop ((n 0))
     (zmq-send requester (string->utf8 "Hello"))
     (let ((ans (zmq-recv requester)))
       (pk 'client)
       (pk (utf8->string ans))    
       (when (< n 10) (loop (+ n 1))))))

